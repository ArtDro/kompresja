import os
import sys
import tkinter as tk
from tkinter import filedialog
from moviepy.editor import VideoFileClip
from tkinter import *
from PIL import Image, ImageTk


class VideoCompressorApp:
    def __init__(self, window):
        self.video_clips = []
        self.master = window

        if getattr(sys, 'frozen', False):
            # W trybie standalone (po skompilowaniu)
            icon_path = os.path.join(sys._MEIPASS, 'icon.ico')
        else:
            # W trybie zwykłego skryptu Pythona
            icon_path = os.path.join(os.path.dirname(sys.argv[0]), 'icon.ico')

        icon_image = Image.open(icon_path)
        self.img = ImageTk.PhotoImage(icon_image)
        self.master.title("Kompresja Wideo")
        self.master.iconphoto(False, self.img)

        # Kolor tła window
        self.master.configure(bg="#404040")

        # Szerokość window
        window_width = 300
        window_height = 250

        # Pobieramy rozmiar ekranu użytkownika
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()

        # Obliczamy współrzędne środka window
        x = (screen_width - window_width) // 2
        y = (screen_height - window_height) // 2

        # Ustawiamy geometrię window na środku pulpitu użytkownika
        self.master.geometry("%dx%d+%d+%d" % (window_width, window_height, x, y))

        # Przycisk do wczytywania pliku
        self.load_button = tk.Button(window,
                                     text="Wczytaj pliki",
                                     bd=5,
                                     command=self.load_file,
                                     bg="#F5DEB3",
                                     activebackground="#FFF8DC",
                                     fg="black",
                                     width=15,
                                     height=2)
        self.load_button.pack(pady=20)
        self.load_button["font"] = ("Arial", 12, "bold")

        # Etykieta informacyjna
        self.status_label_info = tk.StringVar()
        self.status_label = tk.Label(window,
                                     textvariable=self.status_label_info,
                                     bg="#404040",
                                     font=("Arial", 12, "bold"),
                                     fg='white')

        self.status_label.pack()

        # Przycisk do rozpoczęcia kompresji
        self.compress_button = tk.Button(window,
                                         text="Kompresuj",
                                         bd=5,
                                         command=self.compress_video,
                                         bg="#4CAF50",
                                         activebackground="#A5D6A7",
                                         fg="black",
                                         width=15,
                                         height=2)
        self.compress_button.pack(pady=20)
        self.compress_button["font"] = ("Arial", 12, "bold")

    def load_file(self):
        file_types = [("Pliki wideo", "*.mp4;*.avi;*.mov;*.MOV")]
        try:
            file_paths = filedialog.askopenfilenames(title="Wybierz pliki wideo", filetypes=file_types)
            if file_paths:
                self.video_clips = [VideoFileClip(file_path) for file_path in file_paths]
                self.update_status_label()
                self.update_window_size()  # Dodaj to wywołanie po wczytaniu plików
            else:
                self.status_label_info.set("Plik nie został wybrany!")
        except Exception as e:
            self.status_label_info.set(f"Błąd: {e}. Wybrane pliki nie są obsługiwane.")

    def update_status_label(self):
        if self.video_clips:
            file_names = [os.path.basename(clip.filename) for clip in self.video_clips]
            file_list = "\n".join(file_names)
            self.status_label_info.set(f"Pliki załadowane:\n{file_list}\n\nMożesz przejść do kompresji!")
        else:
            self.status_label_info.set("Brak plików do kompresji!")

    def update_window_size(self):
        # Aktualizacja wysokości okna w zależności od ilości plików
        new_height = 300 + len(self.video_clips) * 20
        self.master.geometry(f"{self.master.winfo_width()}x{new_height}")

    def compress_video(self):
        if self.video_clips:
            try:
                self.status_label_info.set("Kompresja w toku ...\nProszę czekać")
                self.master.update()

                for video_clip in self.video_clips:
                    # Wybieramy fragment klipu przed zapisem
                    start_time = 0
                    end_time = video_clip.duration
                    subclip = video_clip.subclip(start_time, end_time)

                    # Utwórz folder "output" w miejscu źródłowym, jeśli nie istnieje
                    output_folder = os.path.join(os.path.dirname(video_clip.filename), "output")
                    if not os.path.exists(output_folder):
                        os.makedirs(output_folder)

                    # Tworzymy ścieżkę wyjściową dla każdego pliku wideo
                    input_path = video_clip.filename
                    output_filename = f"New_{os.path.basename(input_path)}"
                    output_path = os.path.join(output_folder, output_filename)

                    # Zapisujemy wynik do pliku z dodatkowymi parametrami kompresji
                    subclip.write_videofile(output_path, codec="libx264", preset="slow", bitrate="1000k")

                self.status_label_info.set("Kompresja zakończona!")
                self.video_clips = []  # Czyszczenie listy po zakończonej kompresji

                # Otwieranie eksploratora plików w folderze "output" po 2 sekundach
                self.master.after(2000, lambda: os.startfile(output_folder))

                # Zamykanie okna po 2000 milisekundach (2 sekundy)
                self.master.after(2000, self.master.destroy)
            except Exception as e:
                self.status_label_info.set(f"Błąd podczas kompresji wideo: {e}")
        else:
            self.status_label_info.set("Nie wybrano plików do kompresji!")


if __name__ == '__main__':
    master = tk.Tk()
    app = VideoCompressorApp(master)
    master.mainloop()
